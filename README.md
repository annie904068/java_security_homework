1. Framework: Spring Boot、MyBatis、Security

2. 主程式在src/main/java/com/tgl/hw/mvc/WebApplication.java

3. 啟動執行 mvn spring-boot:run

4. 可使用 swagger UI 測試

5. swagger UI: http://localhost:8080/swagger-ui.html

6. 登入後，輸入使用者名稱及密碼，即可使用swagger的try-it-out做CRUD
   
7. 使用者名稱:admin
   密碼:admin