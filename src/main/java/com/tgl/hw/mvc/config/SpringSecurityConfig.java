package com.tgl.hw.mvc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("admin") //具有 ADMIN權限
			.password("{noop}admin")
			.roles("ADMIN")
			.and()
			.withUser("user")
			.password("{noop}user")
			.roles("USER");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.httpBasic()
				.and()
				.authorizeRequests() // 定義哪些URL需要被保護、哪些不需要被保護
				.antMatchers(HttpMethod.GET, "/rest/**").hasAnyRole("ADMIN", "USER")
				.antMatchers(HttpMethod.DELETE, "/rest/**").hasRole("ADMIN")
				.antMatchers(HttpMethod.POST, "/rest/**").hasRole("ADMIN")
				.antMatchers(HttpMethod.PUT, "/rest/**").hasRole("ADMIN")
				.anyRequest() // 任何請求,登入後可以訪問
				.authenticated() // 所有請求都需要登入認證才能訪問
				.and()
				.csrf().disable();
	}
}