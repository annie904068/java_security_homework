package com.tgl.hw.mvc.dao;

import java.util.List;
import com.tgl.hw.mvc.model.Employee;

public interface EmployeeMybatisDao {
	Employee findById(int id);
	
	boolean insert(Employee user);
	
	boolean update(Employee user);
	
	boolean delete(int id);
	
	boolean truncateTable();
	
	boolean batchInsert(List<Employee> employeeList);
	
	
}
