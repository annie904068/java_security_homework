package com.tgl.hw.mvc.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.ResourceUtils;

import com.tgl.hw.mvc.aop.ServiceAspect;

public class EmployeeDataImport {
	private static final Logger LOG = LogManager.getLogger(ServiceAspect.class);

	public List<String> importData() {
		List<String> result = new ArrayList<>();
		// 將resource中的data讀進來
		try (BufferedReader file = new BufferedReader(new FileReader(ResourceUtils.getFile("classpath:employeedata.txt")))) {
			String line = null;
			while ((line = file.readLine()) != null) {
				result.add(line.replace(" ", ",")); //每筆資料的空格換成逗號存入list
			}
		} catch (IOException e) {
			LOG.error("file: employeedata.txt, error: {}", e);
		}
		return result;
	}
}