package com.tgl.hw.mvc.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.tgl.hw.mvc.aop.ServiceAspect;
import com.tgl.hw.mvc.exception.RecordNotFoundException;
import com.tgl.hw.mvc.model.Employee;
import com.tgl.hw.mvc.service.EmployeeMybatisService;
import com.tgl.hw.mvc.util.DataUtil;

@RestController
@RequestMapping(value = "/rest/employee/mybatis")
public class EmployeeMybatisController {
	private static final Logger LOG = LogManager.getLogger(ServiceAspect.class);
	
	@Autowired
	private EmployeeMybatisService employeeService;

	@PostMapping(value = "/insert")
	public ResponseEntity<Employee> insert(@RequestBody Employee employee) {
		employeeService.insert(employee);
		return new ResponseEntity<>(employee, HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Integer> delete(@PathVariable("id") @Min(1) int employeeId) {
		Employee result = employeeService.findById(employeeId);
		if (result == null) {
			throw new RecordNotFoundException(employeeId);
		}
		employeeService.delete(employeeId);
		return new ResponseEntity<>(employeeId, HttpStatus.OK);
	}

	@PutMapping(value = "/update")
	public ResponseEntity<Employee> update(@RequestBody @Valid Employee employee) {
		Employee result = employeeService.findById(employee.getId());
		if (result == null) {
			throw new RecordNotFoundException(employee.getId());
		}
		employeeService.update(employee);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@GetMapping(value = "/getById/{id}")
	public ResponseEntity<Employee> findById(@PathVariable("id") @Min(1) int employeeId) {
		Employee employee = employeeService.findById(employeeId);
		if (employee == null) {
			throw new RecordNotFoundException(employeeId);
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}
	
	@PostMapping(value = "/batchInsert")
	public boolean batchInsert(@RequestParam("file") MultipartFile file ) {
		if (file == null || file.isEmpty()) {
			return false;
		}
		
		List<String> rawList = new ArrayList<>();
	    try (BufferedReader bfReader = new BufferedReader(new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8))) {
	      String line = null;
	      Employee employee;
	      while ((line = bfReader.readLine()) != null) {
	        rawList.add(line);
	        String[] rowData = line.split(",");
			if (rowData.length != 6) {
				LOG.error("file: {}, batchInsert data length error: {}", file.getName(), rowData.toString());
				return false;
			}
			
			employee = new Employee();
			employee.setHeight(Double.valueOf(rowData[0]));
			employee.setWeight(Double.valueOf(rowData[1]));
			employee.setEngName(rowData[2]);
			employee.setChName(rowData[3]);
			employee.setPhone(rowData[4]);
			employee.setEmail(rowData[5]);
			employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
			employeeService.addTolist(employee);
	      }
	    } catch (IOException e) {
	      LOG.error("file: {}, error: {}", file.getName(), e);
	    }
	    return employeeService.batchInsert();
	}
}
