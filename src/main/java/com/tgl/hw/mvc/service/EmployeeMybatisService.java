package com.tgl.hw.mvc.service;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import com.tgl.hw.mvc.dao.EmployeeDataImport;
import com.tgl.hw.mvc.dao.EmployeeMybatisDao;
import com.tgl.hw.mvc.model.Employee;
import com.tgl.hw.mvc.util.DataUtil;

@Service
@Validated
public class EmployeeMybatisService {
	@Autowired
	private EmployeeMybatisDao employeeMybatisDao;

	public boolean insert(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight())); // 計算bmi
		return employeeMybatisDao.insert(employee);
	}

	public boolean delete(int employeeId) {
		return employeeMybatisDao.delete(employeeId);
	}

	public boolean update(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight())); // 計算bmi
		return employeeMybatisDao.update(employee);
	}

	public Employee findById(int employeeId) {
		Employee result = employeeMybatisDao.findById(employeeId); // 提供id，回傳那筆資料
		if (result == null) {
			return null;
		}
		String chName = result.getChName();
		String maskedName = DataUtil.hideName(chName); // 將中文名字做隱碼
		result.setChName(maskedName);
		return result;
	}

	public boolean batchInsertForResourceData(List<String> fileList) {
		List<Employee> empList = new ArrayList<>();
		for(String row: fileList) {
			String[] rowData = row.split(",");
			
			Employee employee = new Employee();
			employee.setHeight(Double.valueOf(rowData[0]));
			employee.setWeight(Double.valueOf(rowData[1]));
			employee.setEngName(rowData[2]);
			employee.setChName(rowData[3]);
			employee.setPhone(rowData[4]);
			employee.setEmail(rowData[5]);
			employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
			empList.add(employee);
		}
		return employeeMybatisDao.batchInsert(empList);
	}
	
	@Valid
	List<Employee> empList = new ArrayList<>();
	
	public void addTolist(@Valid Employee employee) {
		empList.add(employee);
	}
	public  boolean batchInsert() {
		return employeeMybatisDao.batchInsert(empList);
	}
	
	@PostConstruct
	public boolean initTable() {
		employeeMybatisDao.truncateTable(); // 清空資料表
		System.out.println("truncate success");

		EmployeeDataImport empImport = new EmployeeDataImport();
		List<String> emp = empImport.importData(); // 讀檔
		batchInsertForResourceData(emp); // 塞入資料
		System.out.println("employee file input success");
		return true;
	}
}
