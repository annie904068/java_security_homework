package com.tgl.hw.mvc.service;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import com.tgl.hw.mvc.dao.EmployeeJDBCDao;
import com.tgl.hw.mvc.model.Employee;
import com.tgl.hw.mvc.util.DataUtil;

@Service
@Validated
public class EmployeeJDBCService {
	@Autowired
	private EmployeeJDBCDao jdbcDao;

	public Employee insert(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight())); // 計算bmi
		return jdbcDao.insert(employee);
	}

	public boolean delete(int employeeId) {
		return jdbcDao.delete(employeeId);
	}

	public Employee update(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight())); // 計算bmi
		return jdbcDao.update(employee);
	}

	public Employee findById(int employeeId) {
		Employee result = jdbcDao.findById(employeeId); // 提供id，回傳那筆資料
		if (result == null) {
			return null;
		}
		String chName = result.getChName();
		String maskedName = DataUtil.hideName(chName); // 將中文名字做隱碼
		result.setChName(maskedName);
		return result;
	}
	
	@Valid
	List<Employee> empList = new ArrayList<>();
	
	public void addTolist(@Valid Employee employee) {
		empList.add(employee);
	}
	public boolean batchInsert() {
		return jdbcDao.batchInsert(empList);
	}
}
